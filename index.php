<?php
// Create database connection using config file
include_once("config.php");

// Fetch all users data from database
$result = mysqli_query($mysqli, "SELECT a.id, a.name,a.email,a.mobile,b.nama_sekolah
from users a
left join sekolah b
on a.id_sekolah= b.id_sekolah
order by a.id desc");
?>

<html>
<head>    
    <title>Homepage</title>
</head>

<body>
<a href="add.php">Add New User</a>
<a href="add new sekolah.php">Add New sekolah</a><br/><br/>
    <table width='80%' border=1>
<?php $no = 1;?>
    <tr>
        <th>No</th> <th>Name</th> <th>Mobile</th> <th>Email</th> <th>Nama Sekolah</th> <th>Update</th>
    </tr>
    <?php  
    while($user_data = mysqli_fetch_array($result)) {         
        echo "<tr>";
        echo "<td>".$no++."</td>";
        echo "<td>".$user_data['name']."</td>";
        echo "<td>".$user_data['mobile']."</td>";
        echo "<td>".$user_data['email']."</td>"; 
        echo "<td>".$user_data['nama_sekolah']."</td>";
        echo "<td><a href='edit.php?id=$user_data[id]'>Edit</a> | <a href='delete.php?id=$user_data[id]'>Delete</a> |<a href='detail.php?id=$user_data[id]'>Detail</a></td></tr>";       
    }
    ?>
    </table>
</body>
</html>
